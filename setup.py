import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="broadmap",
    version="0.0.1",
    author="Brandon Burnett",
    author_email="brandon@awesomeindustries.net",
    description="A small utility to query demographics from broadbandmap.gov",
    long_description=long_description,
    long_description_content_type="text/markdown",
    scripts=['bin/broadmap'],
    url="",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
