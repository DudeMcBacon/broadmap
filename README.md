# broadmap
A small utility for querying demographic information from broadbandmap.gov

## Assumptions
* I have built the application against Python 3.7.0 and have not vetted it
against older version of Python due to time constraints. I do know that there
is a hard requirement to have at least Python 3.6 because I've made liberal
use of literal string interpolation which was introduced in that version. With
not too much re-work I think this could fairly easily support every major
version of Python 3, at least.
* I have abstracted the code for displaying demographic information into
it's own class. The assumption being that this class might be used in more
than one place.
* When using the command-line utility, all state names are normalized in
lowercase by the command-line parser. This allows state names to be
specified on the command line in any capitalization or case.
* States with spaces in their names must be supplied via the command-line in
quotations. E.g., "new york" for New York.
* I have written basic tests for the Broadmaps class but I have not done
any testing of the command-line utility I've included. I'm working under the
assumption that this is acceptable for a first pass.
* I have not put a lot of thought into the performance of this package. There
are some basic things we could do (which I've noted as comments in the code) but
otherwise I've been writing the code with the assumption that returning results
in 60 seconds are less is appropriate.
* I've raised all exceptions up from the Broadmaps class for handling in the
command-line utility. With a better understanding of how the API behaves it
may be possible to handle them from within the class but I've left that as an
exercise for another day.

## Installation
This application was built targeting Python 3.7.0 which is not too difficult
to get up and running if you're on a MacBook with Homebrew. I've included
installing Python 3.7 in this set of instructions just for my own future
reference. If you already have Python 3.7, you can skip this set of
instructions.

### Install Python 3.7.0
These instructions should get you up and running with Python 3.7.0

```
$ brew install pyenv
$ pyenv install 3.7.0   # Install Python 3.7.0
$ pyenv init            # Initialize pyenv
$ pyenv shell 3.7.0     # Set the current shell to use Python 3.7.0
```

### Prepare pyenv, install dependencies, etc
The following instructions show how to work with pyenv, install dependencies,
and prepare your shell environment to run broadmap.

```
$ git clone https://DudeMcBacon@bitbucket.org/DudeMcBacon/broadmap.git
$ cd broadmap
$ pip install pipenv   # Install pipenv via Pip
$ pipenv install       # Install dependencies from Pipfile
$ pipenv shell         # Activate the virtualenv
$ pip install -e .     # Install the package from the local directory
$ bin/broadmap --help
```

## Usage
Get the average income below poverty for Oregon and Washington:
```
$ broadmap oregon Washington --output averages
```

Get the demographics of oregon and washington in CSV format:
```
$ broadmap Oregon washington --output CSV
```

Basic Usage:
```
$ broadmap --help
usage: broadmap [-h] [--output [OUTPUT]] [--verbose] [--no-verbose] S [S ...]

Output some demographics about states in the US

positional arguments:
  S                  States to return demographic info for

optional arguments:
  -h, --help         show this help message and exit
  --output [OUTPUT]  The output type to return (CSV, averages)
  --verbose          Enable verbose logging
  --no-verbose       Disable verbose logging
```
