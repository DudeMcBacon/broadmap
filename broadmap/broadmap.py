from operator import itemgetter
import logging
import requests

class Broadmap:

    API_BASE = 'https://www.broadbandmap.gov/broadbandmap'

    def __init__(self, states, level=logging.ERROR):
        logging.basicConfig(level=level)
        logging.info(f'Initialized with states: {", ".join(states)}')

        # TODO:
        # The us module contains FIPS IDs already so we could potentially
        # save ourselves a bunch of API calls here. However, the directions
        # stated I should use the broadbandmap API to get the FIPS ID so I have
        # not done that.
        self.fips = list(map(self.__get_fips_id, states))
        logging.info(f'FIPS IDs are: {", ".join(self.fips)}')

    def averages(self):
        """
        Averages the incomes below poverty of the given FIPS IDs and returns a
        float.
        """

        demos = self.__get_demographics_by_ids(self.fips)
        incomesBelowPoverty = [state["incomeBelowPoverty"] for state in demos]
        return sum(incomesBelowPoverty) / len(incomesBelowPoverty)

    def csv(self):
        """
        Retrieves the demographics of the given FIPS IDs and returns them as a
        CSV formatted string.
        """

        demos = self.__get_demographics_by_ids(self.fips)
        sorted_demos = sorted(demos, key=itemgetter('geographyName'))
        lines = []
        for state in sorted_demos:
            lines.append(f'{state["geographyName"]}, {state["population"]}, {state["households"]}, {state["incomeBelowPoverty"]}, {state["medianIncome"]}')
        return '\n'.join(lines)

    def __get_demographics_by_ids(self, ids):
        """
        Queries the broadbandmaps API for the given IDs and returns a list of
        hashes representing each ID.
        """

        # According to the documentation for this API, the maximum number of
        # IDs that can be supplied is ten. This logic pulls ten IDs at a time
        # from the list of IDs supplied to the instance, gets the demograhics,
        # and then appends them to a list. The list will be returned by this
        # method once all of the demographics have been pulled.
        local_fips = self.fips.copy()
        results = []
        while len(local_fips) > 0:
            # Create a string of the first ten elements of local_fips and
            # remove them from the local_fips list.
            id_string = ','.join(local_fips[:10])
            del local_fips[:10]

            # GET the demographics for the id_string generated above
            logging.info(f'GETing Demographics for FIPS IDs: {id_string}')
            url = f'{self.API_BASE}/demographic/jun2014/state/ids/{id_string}?format=json'
            logging.info(f'GETing Demographics from {url}')
            try:
                r = requests.get(url)
            except requests.exceptions.RequestException as e:
                raise e
            logging.info(f'{url} responded with: {r.json()}')

            # Append each element of the results array to the results variable
            # so we don't end up with a list of lists.
            [results.append(result) for result in r.json()['Results']]
        return results

    def __get_fips_id(self, state):
        """
        Queries the broadbandmaps API for the FIPS ID of the given state and
        returns an integer.
        """

        logging.info(f'GETing FIPS ID for {state}')
        url = f'{self.API_BASE}/census/state/{state}?format=json'
        logging.info(f'GETing FIPS ID from {url}')
        try:
            r = requests.get(url)
        except requests.exceptions.RequestException as e:
            raise e
        logging.info(f'{url} responded with: {r.json()}')
        return r.json()['Results']['state'][0]['fips']

