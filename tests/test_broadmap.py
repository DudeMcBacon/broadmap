import json
import logging
import broadmap
import unittest
import sure
import httpretty

class TestBroadmap(unittest.TestCase):
    """
    Test the functions from the broadmap class
    """

    @httpretty.activate
    def test_csv(self):
        url = "https://www.broadbandmap.gov/broadbandmap/census/state/oregon?format=json"
        body = '{"status":"OK","responseTime":21,"message":[],"Results":{"state":[{"geographyType":"STATE2010","name":"Oregon","fips":"41","stateCode":"OR"}]}}'
        httpretty.register_uri(httpretty.GET, url, body=body,
                content_type="application/json")

        url2 = "https://www.broadbandmap.gov/broadbandmap/demographic/jun2014/state/ids/41?format=json"
        body2 = '{"status":"OK","responseTime":91,"message":[],"Results":[{"geographyId":"41","geographyName":"Oregon","landArea":96098.56583654,"population":3996309,"households":1779290,"raceWhite":0.84440,"raceBlack":0.01150,"raceHispanic":0.11050,"raceAsian":0.02830,"raceNativeAmerican":0.00530,"incomeBelowPoverty":0.15940,"medianIncome":53775.86490,"incomeLessThan25":0.24220,"incomeBetween25to50":0.26330,"incomeBetween50to100":0.31910,"incomeBetween100to200":0.14570,"incomeGreater200":0.02980,"educationHighSchoolGraduate":0.84440,"educationBachelorOrGreater":0.25240,"ageUnder5":0.05360,"ageBetween5to19":0.19450,"ageBetween20to34":0.19190,"ageBetween35to59":0.32650,"ageGreaterThan60":0.23340,"myAreaIndicator":false}]}'
        httpretty.register_uri(httpretty.GET, url2, body=body2,
                content_type="application/json")

        bmap = broadmap.Broadmap(['oregon'])
        averages = bmap.csv()
        averages.should.be.equal('Oregon, 3996309, 1779290, 0.1594, 53775.8649')

    @httpretty.activate
    def test_averages(self):
        url = "https://www.broadbandmap.gov/broadbandmap/census/state/oregon?format=json"
        body = '{"status":"OK","responseTime":21,"message":[],"Results":{"state":[{"geographyType":"STATE2010","name":"Oregon","fips":"41","stateCode":"OR"}]}}'
        httpretty.register_uri(httpretty.GET, url, body=body,
                content_type="application/json")

        url2 = "https://www.broadbandmap.gov/broadbandmap/demographic/jun2014/state/ids/41?format=json"
        body2 = '{"status":"OK","responseTime":91,"message":[],"Results":[{"geographyId":"41","geographyName":"Oregon","landArea":96098.56583654,"population":3996309,"households":1779290,"raceWhite":0.84440,"raceBlack":0.01150,"raceHispanic":0.11050,"raceAsian":0.02830,"raceNativeAmerican":0.00530,"incomeBelowPoverty":0.15940,"medianIncome":53775.86490,"incomeLessThan25":0.24220,"incomeBetween25to50":0.26330,"incomeBetween50to100":0.31910,"incomeBetween100to200":0.14570,"incomeGreater200":0.02980,"educationHighSchoolGraduate":0.84440,"educationBachelorOrGreater":0.25240,"ageUnder5":0.05360,"ageBetween5to19":0.19450,"ageBetween20to34":0.19190,"ageBetween35to59":0.32650,"ageGreaterThan60":0.23340,"myAreaIndicator":false}]}'
        httpretty.register_uri(httpretty.GET, url2, body=body2,
                content_type="application/json")

        bmap = broadmap.Broadmap(['oregon'])
        averages = bmap.averages()
        averages.should.be.equal(0.1594)


    @httpretty.activate
    def test_get_demographics_by_ids(self):
        url = "https://www.broadbandmap.gov/broadbandmap/census/state/oregon?format=json"
        body = '{"status":"OK","responseTime":21,"message":[],"Results":{"state":[{"geographyType":"STATE2010","name":"Oregon","fips":"41","stateCode":"OR"}]}}'
        httpretty.register_uri(httpretty.GET, url, body=body,
                content_type="application/json")

        url2 = "https://www.broadbandmap.gov/broadbandmap/demographic/jun2014/state/ids/41?format=json"
        body2 = '{"status":"OK","responseTime":91,"message":[],"Results":[{"geographyId":"41","geographyName":"Oregon","landArea":96098.56583654,"population":3996309,"households":1779290,"raceWhite":0.84440,"raceBlack":0.01150,"raceHispanic":0.11050,"raceAsian":0.02830,"raceNativeAmerican":0.00530,"incomeBelowPoverty":0.15940,"medianIncome":53775.86490,"incomeLessThan25":0.24220,"incomeBetween25to50":0.26330,"incomeBetween50to100":0.31910,"incomeBetween100to200":0.14570,"incomeGreater200":0.02980,"educationHighSchoolGraduate":0.84440,"educationBachelorOrGreater":0.25240,"ageUnder5":0.05360,"ageBetween5to19":0.19450,"ageBetween20to34":0.19190,"ageBetween35to59":0.32650,"ageGreaterThan60":0.23340,"myAreaIndicator":false}]}'
        httpretty.register_uri(httpretty.GET, url2, body=body2,
                content_type="application/json")

        bmap = broadmap.Broadmap(['oregon'])
        demos = bmap._Broadmap__get_demographics_by_ids(['41'])
        demos.should.be.equal([json.loads(body2)['Results'][0]])

    @httpretty.activate
    def test_get_fips_id(self):
        url = "https://www.broadbandmap.gov/broadbandmap/census/state/oregon?format=json"
        body = '{"status":"OK","responseTime":21,"message":[],"Results":{"state":[{"geographyType":"STATE2010","name":"Oregon","fips":"41","stateCode":"OR"}]}}'
        httpretty.register_uri(httpretty.GET, url, body=body,
                content_type="application/json")

        bmap = broadmap.Broadmap(['oregon'])
        fips = bmap._Broadmap__get_fips_id('oregon')
        fips.should.be.equal('41')

if __name__ == '__main__':
    unittest.main()
